import argparse
from configparser import ConfigParser
import os
import sys
import traceback

from .const import CONFIG_FILENAME

from .models.Bot import Bot
from .models.DiscordConnector import DiscordConnector
from .models import Logger


def getConfig(env):
    config = ConfigParser(converters = { "int": lambda x: int(x) })
    config.read(CONFIG_FILENAME)
    return config[env]

def setup():
    if not os.path.isdir('data'):
        os.mkdir('data')

    with open('VERSION') as f:
        print("PictureGame Bot {} by Provium\n------\n".format(f.read().strip()))

    parser = argparse.ArgumentParser(description="The /r/PictureGame Reddit Bot.")

    parser.add_argument("--env", type = str, required = True,
        help = "Name of the environment to use from bot.conf")
    parser.add_argument("--logConfig", type = str,
        help = "Name of the file from which to load logging config")

    args = parser.parse_args()

    valid = True

    for fileName in [args.logConfig, CONFIG_FILENAME]:
        if fileName is not None and not os.path.isfile(fileName):
            print("File not found:", fileName)
            valid = False

    if not valid:
        sys.exit()

    config = getConfig(args.env)
    discord = DiscordConnector(config)

    logConfigFile = args.logConfig
    if logConfigFile is None:
        logConfigFile = config["logConfig"]
    Logger.setupLogging(logConfigFile, config, discord)

    version = '99.99.99-dev'
    with open('VERSION') as versionFile:
        version = versionFile.read().strip()

    bot = Bot(config, version, discord)

    Logger.info("Setup successful")

    return bot


def main():
    bot = setup()

    try:
        bot.mainLoop()

    except KeyboardInterrupt:
        print("\nExitting...")
        Logger.flush()

    except Exception as e:
        Logger.fatal(traceback.format_exc())
        raise e

