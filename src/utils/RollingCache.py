from queue import SimpleQueue

class RollingCache:
    def __init__(self, capacity):
        self.capacity = capacity
        self.queue = SimpleQueue()
        self.dict = {}

    def Add(self, key, value):
        if self.queue.qsize() >= self.capacity:
            toEvict = self.queue.get_nowait()
            self.dict.pop(toEvict)

        self.queue.put_nowait(key)
        self.dict[key] = value

    def Get(self, key):
        return self.dict.get(key)
