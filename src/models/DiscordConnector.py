import json
import socket

from . import Logger
from ..utils.RollingCache import RollingCache

class DiscordConnector:
    def __init__(self, config):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.addr = config.get("discordAddress", "localhost")
        self.port = int(config.get("discordPort", 12345))
        self.commentAuthors = RollingCache(1000)

    def send(self, message):
        self.socket.sendto(bytes(json.dumps(message), "utf-8"), (self.addr, self.port))

    def reportRoundStatus(self, data):
        Logger.info("Sending communication to discord for round status", { "status": data["status"] }, sendToDiscord = False)
        data["type"] = "current-round"
        self.send(data)

    def getOpCommentParentAuthor(self, comment):
        # Discord bot only needs to know the author of the comment's parent for OP's comments
        # This is to determine if it should notify about OP's comment or not:
        # Comments replying to other users are most likely just reponses to guesses, so no notification is necessary.
        # Since determining the parent author may require additional requests to the Reddit API, we avoid it where it's not necessary
        if comment.is_submitter:
            if comment.is_root:
                return comment.link_author
            else:
                # Check the cache to see if we recently saw the parent comment and determined its author
                # Note: The first call to parent() to get the id does not do a reddit request; this just gives us the id without the t1_ prefix
                # We expect that _usually_, OP will be replying to recent comments, in which case they will still be cached
                parent = comment.parent()
                return self.commentAuthors.Get(parent.id) or (parent.author.name if parent.author else None)

        return None

    def reportNewComment(self, comment):
        Logger.debug("Sending communication to discord for comment", { "id": comment.id }, sendToDiscord = False)

        self.commentAuthors.Add(comment.id, comment.author.name)

        self.send({
            "type": "comment",
            "author": comment.author.name,
            "isOp": comment.is_submitter,
            "postId": comment.submission.id,
            "commentId": comment.id,
            "parentId": comment.parent_id,
            "parentAuthor": self.getOpCommentParentAuthor(comment),
            "body": comment.body,
        })

    def reportEditedComment(self, comment):
        Logger.debug("Sending communication to discord for edited comment", { "id": comment.id, "edited": comment.edited }, sendToDiscord = False)

        self.send({
            "type": "comment-edit",
            "author": comment.author.name,
            "isOp": comment.is_submitter,
            "postId": comment.submission.id,
            "commentId": comment.id,
            "parentId": comment.parent_id,
            "parentAuthor": self.getOpCommentParentAuthor(comment),
            "body": comment.body,
            "edited": comment.edited,
        })

    def reportModPost(self, submission):
        Logger.info("Sending communication to discord for mod post", { "id": submission.id }, sendToDiscord = False)
        self.send({
            "type": "mod-post",
            "author": submission.author.name,
            "title": submission.title,
            "postId": submission.id,
        })

    def reportModMail(self, message):
        Logger.info("Sending communication to discord for modmail message", { "conversation": message["conversation"] }, sendToDiscord = False)
        self.send({
            "type": "mod-mail",
            "conversation": message["conversation"],
            "subject": message["subject"],
            "author": message["author"],
            "body": message["body"],
        })

    def reportRoundCorrection(self, comment, targetComment, roundNumber):
        Logger.info("Sending communication to discord for round correction", {
            "submission": targetComment.submission.id,
            "comment": targetComment.id,
            "roundNumber": roundNumber,
        }, sendToDiscord = False)
        self.send({
            "type": "round-correction",
            "ordinal": 0,
            "commentId": targetComment.id,
            "submissionId": targetComment.submission.id,
            "correcter": comment.author.name,
            "winner": targetComment.author.name,
            "roundNumber": roundNumber,
        })

    def reportCompletedRoundCorrection(self, comment, roundNumber):
        Logger.info("Sending communication to discord for completed round correction", {
            "submission": comment.submission.id,
            "comment": comment.id,
            "roundNumber": roundNumber,
        }, sendToDiscord = False)
        self.send({
            "type": "round-correction",
            "ordinal": 1,
            "commentId": comment.id,
            "submissionId": comment.submission.id,
            "winner": comment.author.name,
            "winTime": comment.created_utc,
            "roundNumber": roundNumber,
        })

    def reportLogMessage(self, level, message, data):
        self.send({
            "type": "log",
            "level": level,
            "message": message,
            "data": data
        })
