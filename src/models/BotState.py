from . import Logger

class BotState:
    def __init__(self, currentRound, previousRound):
        self.roundId = currentRound["id"]
        self.roundNumber = currentRound["roundNumber"]

        currentRoundEndTime = currentRound.get("winTime") or currentRound.get("abandonedTime")
        self.ongoingRound = currentRoundEndTime is None

        self.prevRoundId = previousRound["id"]
        self.prevRoundEndTime = currentRoundEndTime or previousRound.get("winTime") or previousRound.get("abandonedTime")

        self.missingThumbnail = currentRound.get("thumbnailUrl", "default") == "default"

        if self.ongoingRound:
            self.currentHost = currentRound["hostName"]
        else:
            self.roundNumber += 1
            self.currentHost = currentRound.get("winnerName")

        Logger.info("State initialized", {
            "roundNumber": self.roundNumber,
            "url": "https://redd.it/" + self.roundId,
            "ongoing": self.ongoingRound,
        })
